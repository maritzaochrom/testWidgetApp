import React from "react";
import { Route, useParams } from "react-router-dom";
import './App.css';
import LoginDashboard from './components/LoginDashboard';


function App() {


  return (

      <div className="App">

        <LoginDashboard />

      </div>
  );
}

export default App;
