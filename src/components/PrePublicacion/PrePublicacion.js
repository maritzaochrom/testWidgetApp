import { prettyDOM } from '@testing-library/react';
import React, { useState } from 'react';
import  './PrePublicacion.css';
import ReleaseNews from './ReleaseNews';
import moment from 'moment';

const PrePublicacion  = (props) => {

    // Recibimos las props (que es un objeto con toda la informacion de prePublicacion) y lo declaramos en la variable con el nombre prePublicacion
    const prePublicacion = props.prePublicacion;

    // const datesArray = props.prePublicacion[0];
    // let date = datesArray.fh_comunicado;

    return (
        <div>
            {/* Esto se va a eliminar cuando cambie la lambda, Mientras el objeto no sea cero, renderea o regresa a pantalla todo el siguiente codigo */}
            { Object.keys(prePublicacion).length !== 0  ?

                 <div className="Publicacion">
                   <p className="ContenedorText">Hello, Freight Forwarder</p>

                   <div className="Head">
                        <div className="Hairline"></div>
                        <div className="Hours">{moment(props.prePublicacion[0].fh_comunicado).fromNow()}</div>
                   </div>

                    {/* Manda a llamar al componente ReleaseNews mandando props de prePublicacion con el mismo nombre, las props se envían como un objeto {} */}
                    <ReleaseNews prePublicacion={prePublicacion} datePrePub={props.datePrePub} />

                    <div className="Foot">
                        <div className="HairlineBottom"></div>

                    </div>

                 </div>


            : null }

        </div>

    )
}

export default PrePublicacion;
