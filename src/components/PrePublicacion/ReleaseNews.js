import React from 'react';
import DOMPurify  from 'dompurify';
import { Scrollbars } from 'react-custom-scrollbars-2';
import  './PrePublicacion.css';


const ReleaseNews = (props) => {

    // Recibimos las props del componente PrePublicacion.js (las props aqui es un arreglo de objetos con toda la informacion de prePublicacion, [{},{},{}])
    const prePublicacion = props.prePublicacion;
    const prePubDateCreated = props.datePrePub;

    // Vamos a utlizar la prePublicacion mas actual por eso utilizamos la posicion cero del arreglo [0]
    // variables parrafoUno y parrafoDos, las declaramos porque necesitamos sanitizar esta informacion
    const parrafoUno = props.prePublicacion[0].tx_parrafoUno;
    const parrafoDos = props.prePublicacion[0].tx_parrafoDos;


    console.log(parrafoUno)


    let h2ParrafoUno = document.getElementsByTagName("h2")

    console.log(h2ParrafoUno)
    let arrH2ParrafoUno = [];
    console.log(h2ParrafoUno)
    for (let x=0; x<h2ParrafoUno.length; x++) {
        //h2ParrafoUno[x].innerHTML === "Mexico" ?  <img src="https://img.icons8.com/emoji/48/000000/mexico-emoji.png"/> : console.log(false);
        h2ParrafoUno[x].className="TitleCountry"
        arrH2ParrafoUno.push(h2ParrafoUno[x].innerHTML)

    }


    return (
        <div>

            <Scrollbars style={{ width: 695 ,height: 450}}>
                <div className="NewsTitle">
                    <h1 className="NewRelease"> {prePublicacion[0].tx_titulo.slice(0,15)} </h1>
                    <p className="Time"> {prePubDateCreated} </p>

                </div>

                <div className="News">
                    <p> {prePublicacion[0].tx_subTitulo} </p>
                </div>


                <div className="ColumnNewFeatures">
                    <h3 className="New"> New</h3>

                    <div>
                        <div className="TextGlobal">
                            {/* Usamos la libreria DOMPurify para sanitizar el parrafoUno, de decir para respetar el estilo y etiquetas HTML  */}
                            <div dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(parrafoUno)}}></div>
                        </div>
                    </div>
                </div>


                <div className="ColumnBugs">
                    <h3 className="Bug">Bug fixes</h3>

                    <div>
                        <div className="TextGlobalBug">
                             {/* Usamos la libreria DOMPurify para sanitizar el parrafoDos, de decir para respetar el estilo y etiquetas HTML  */}
                            <div dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(parrafoDos)}}></div>

                        </div>
                    </div>

                </div>

            </Scrollbars>

        </div>
    )
}

export default ReleaseNews;
