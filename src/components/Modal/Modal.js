import React from 'react';
import './Modal.css';

const Modal = ({ isOpen, closeModal, children }) => {

    const handleModalDialogClick = (e) => {
        e.stopPropagation();
    }

    return (
        <div className={`modal ${isOpen && 'modal-open'}`} onClick={closeModal}>
            <div onClick={handleModalDialogClick}>

                {/* <button onClick={closeModal}>
                    Close Modal
                </button> */}

                {children}

            </div>
        </div>
    )
}

export default Modal;
