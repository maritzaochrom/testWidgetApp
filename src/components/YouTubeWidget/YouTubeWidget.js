import React from 'react';
import { ElfsightWidget } from 'react-elfsight-widget';
import './YouTubeWidget.css'

const YouTubeWidget = (props) => {

    // Recibimos como props el nombre del cliente
    const client = props.client;

    return (
        <div>
            <p className="TitleVideoContent">VIDEO CONTENT</p>

            {(() => {

                switch(client) {

                    // En caso de que el cliente sea schryver, renders en la pantalla el componente llamado Elfsight con el ID correspodiente al cliente
                    case "schryver":
                       return (
                            <div className="Videos">
                                {/* En caso de que quieras cambiar el origen de la informacion de YouTube del cliente, aqui solo se modificaría el ID que te da la pagina de Elsight para este widget */}
                                 <ElfsightWidget widgetID="b7c5f7a3-d72b-4222-9d94-8d9ef0595ace" />

                            </div>
                        )
                       break;

                    case "alpha":
                        return (
                            <div className="Videos">
                                 {/* <ElfsightWidget widgetID="8ab0e618-a1a3-4ba1-971f-78454211ae6d" /> */}
                            </div>
                        )
                        break;

                    default:
                        break;
                }
            })()}

        </div>
    )
}

export default YouTubeWidget;


