import React, { useState, useEffect } from 'react';
import './LoginDashboard.css';
import { Container, Row } from 'react-bootstrap';
import * as api from './api';
import BandejaDeAvisos from './BandejaDeAvisos/BandejaDeAvisos';


const LoginDashboard = () => {

    // Gets the entire query string  If the URL is "http://dev.widgets.com:3000?client=schryver" result is:  ?client=schryver
    const querystring = window.location.search

    // Gets the "client" parameter from the URL, If the URL is "http://dev.widgets.com:3000?client=schryver"  result is: schryver
    const params = new URLSearchParams(querystring)
    const client = params.get('client')


    // Hook useState from React
    const [formData, setFormData] = useState({
        email: "",
        password: "",
      });

    //Hooks for the 3 publications to {} empty object to the new info when call the api - lambda
    const [postPublicacion, setPostPublicacion] = useState({})
    const [prePublicacion, setPrePublicacion] = useState({})
    const [noticia, setNoticia] = useState({})


    useEffect(() => {
        api.getInfo().then((response) => {
            //set the hooks with the info from the lambda,  filter all the post who the plantilla calls  "Pre-publicacion"
            setPrePublicacion(response.data.recordset.filter((post) => post.nb_plantilla === "Pre-publicacion"))

            //set the hooks with the info from the lambda,  filter all the post who the plantilla calls  "Post-publicacion" and gets the lastone
            const arrayPostPublicacion = response.data.recordset.filter((post) => post.nb_plantilla === "Post-publicacion")
            arrayPostPublicacion.length === 0 ? setPostPublicacion({}) : setPostPublicacion(arrayPostPublicacion[arrayPostPublicacion.length-1])

            //set the hooks with the info from the lambda,  filter all the post who the plantilla calls  "Noticia"
            setNoticia(response.data.recordset.filter((post) => post.nb_plantilla === "Noticia"))

        })

      }, []);


    return (

        <div>

            {/* RENDERS ALL THE PUBLICATIONS and send the props or (all info from publication) as an object */}
            <BandejaDeAvisos prePublicacion={prePublicacion}  postPublicacion={postPublicacion} noticia={noticia} client={client} />


        </div>


    )
}

export default LoginDashboard;
