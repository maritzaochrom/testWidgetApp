import React from 'react';
import DOMPurify  from 'dompurify';
import { Scrollbars } from 'react-custom-scrollbars-2';
import './Noticia.css';
import moment from 'moment';

const Noticia = (props) => {

    // Recibimos las props del componente BandejaDeAvisos con los nombre de "noticia" y "recentNews"
    // Aqui inicializamos con variables nuestras props
    const noticia = props.noticia;
    const postNumber = props.recentNews;

    let titulo, parrafo, tituloCapitalize, sanitizedTitulo;

    var temp = document.createElement("div");


    // Se va a eliminar cuando cambie la lambda
    // Si el objeto de noticia no esta vacio, inicializa las varialbes titulo y parrafo
    if (Object.keys(noticia).length !== 0 ) {
        titulo =  noticia[postNumber].tx_titulo;
        temp.innerHTML = titulo;
        sanitizedTitulo = temp.textContent || temp.innerText;
        console.log(sanitizedTitulo)
        sanitizedTitulo.replace(/[^a-zA-Z0-9]/g, '')
        tituloCapitalize = sanitizedTitulo[0].toUpperCase() + sanitizedTitulo.slice(1).toLocaleLowerCase();
        parrafo = noticia[postNumber].tx_parrafoUno;
        console.log(tituloCapitalize)
    }


    return (

        <div>

        {/* Mientras que el objeto noticia no este vacio muestra en el navegador todo el siguiente codigo */}
        { Object.keys(noticia).length !== 0  ?

                <div className="PopupNews">
                <p className="GreetingsNews">Hello, Freight Forwarder</p>
                    <div className="PopupHead">

                        <p className="DateNewsCreated">{moment(props.noticia[postNumber].fh_comunicado).fromNow()}</p>
                        <div className="HairlineNews"></div>
                    </div>

                    <div className ="ScrollArea">
                        <Scrollbars style={{ width: 695 ,height: 450}}>
                            <img
                                className="VideoNews"
                                src={noticia[postNumber].tx_imagenUno}
                                alt="Second slide"
                            />
                            <div className="ContentNews">
                                {/* Para sanitizar usamos una libreria DOMPurify */}
                                {/* Muestra el HTML real respetando el estilo */}
                                <div className="TitleVideo" dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(titulo)}}></div>
                                <div dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(parrafo)}}></div>
                            </div>
                        </Scrollbars>

                    </div>

                    <div className="FooterNews">
                        <div className="HairlineBottomNews"></div>
                    </div>
                </div>


         : null }
    </div>


    )
}

export default Noticia;
