import React from 'react';
import { ElfsightWidget } from 'react-elfsight-widget';
import './SocialMediaWidget.css';
import { Scrollbars } from 'react-custom-scrollbars-2';

const SocialMediaWidget = (props) => {

    // Receives the props from BandejaDeAvisos, the client´s name
    const client = props.client;

    return (
        <div>
            <div className="TitleSocialMedia">SOCIAL MEDIA CONTENT</div>

            {(() => {

                switch(client) {
                    // In case the client is "schryver", renders the instagram of this company.
                    case "schryver":
                       return (
                        <div>
                             {/* <Scrollbars style={{ width: 815 ,height: 805 }}> */}
                                <div className="PostsSocialMedia">
                                        {/* You only need to change the widget ID in case you want to put another instagran widget */}
                                         <ElfsightWidget widgetID="9db6571e-0bd6-44b7-85f9-b4aa1db6b330" />

                                        {/* <script src="https://apps.elfsight.com/p/platform.js" defer></script>
                                            <div class="elfsight-app-9db6571e-0bd6-44b7-85f9-b4aa1db6b330"></div> */}

                                        {/* https://www.positive.news/feed/ */}
                                </div>

                            {/* </Scrollbars> */}

                         </div>

                       )
                    break;

                    case "alpha":
                        return (
                            <div className="PostsSocialMedia">
                                {/* <ElfsightWidget widgetID="a3c8fefb-3cab-4399-a920-e4c7c7e3589b" /> */}
                            </div>
                        )
                        break;

                    default:
                        break;
                }
            })()}

        </div>
    )

}

export default SocialMediaWidget;

