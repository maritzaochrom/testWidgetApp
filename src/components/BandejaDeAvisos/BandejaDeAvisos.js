import React from 'react';
import './BandejaDeAvisos.css';
import PrePublicacion from '../PrePublicacion/PrePublicacion';
import PostPublicacion from '../PostPublicacion/PostPublicacion';
import Noticia from '../Noticia/Noticia';
import YouTubeWidget from '../YouTubeWidget/YouTubeWidget';
import SocialMediaWidget from '../SocialMediaWidget/SocialMediaWidget';
import useModal from '../hooks/useModal';
import Modal from '../Modal/Modal';
import moment from 'moment';

const BandejaDeAvisos = (props) => {

  const client = props.client;
  const prePublicacion = props.prePublicacion;
  const postPublicacion = props.postPublicacion;
  const noticia = props.noticia;
  let prePubDateCreated,postPubDateCreated, noticia1DateCreated, noticia2DateCreated ;


  if (Object.keys(prePublicacion && postPublicacion && noticia).length !== 0) {
       prePubDateCreated = prePublicacion[0].fh_comunicado;
       postPubDateCreated = postPublicacion.fh_comunicado;
       noticia1DateCreated = noticia[0].fh_comunicado;
       noticia2DateCreated = noticia[1].fh_comunicado;

  }

  const [isOpenModalPrePub, openModalPrePub, closeModalPrePub] = useModal();
  const [isOpenModalPostPub, openModalPostPub, closeModalPostPub] = useModal();
  const [isOpenModalNoticia1, openModalNoticia1, closeModalNoticia1] = useModal();
  const [isOpenModalNoticia2, openModalNoticia2, closeModalNoticia2] = useModal();


  function convertDateFormat(date) {
    var dt = new Date(date);
    var h =  dt.getHours(), m = ("0" + dt.getMinutes()).slice(-2);
    var _time = (h > 12) ? (h-12 + ":" + m +" PM") : (h + ":" + m +" AM");
    return date.getDate()  + "." + (date.getMonth()+1) + "." + date.getFullYear() + " "  + _time;
  }


    return (

      <div>
         { Object.keys(prePublicacion).length !== 0  ?

          <div>

            <div>
              <div >
                  <p className="Welcome">Welcome </p>
              </div>

              <div className="GridContainer">
                  <div className="Bandejadeavisos">
                      <p className="Communication">Communication</p>

                      <button className="AvisoItemNuevo" onClick={openModalPrePub}>
                        <p className="TitleNewRelease">🚀 TMS New Release</p>
                        <p className="TimeRelease">{moment(prePubDateCreated).fromNow()}</p>
                        <p className="DateRelease">{convertDateFormat(new Date(prePubDateCreated))} (CDT)</p>
                      </button>

                      <button className="AvisoItemViejo1" onClick={openModalPostPub} >
                          <p className="TitleCookieTime">🍪 It’s cookie time!</p>
                          <p className="TimeCookieTime">{moment(postPubDateCreated).fromNow()}</p>
                          <p className="DateCookieTime">{convertDateFormat(new Date(postPubDateCreated))}  (CDT)</p>
                      </button>

                      <button className="AvisoItemViejo2" onClick={openModalNoticia1}>
                        <p className="TitleVideo2">🎥 New company video </p>
                        <p className="TimeVideo2">{moment(noticia1DateCreated).fromNow()}</p>
                        <p className="DateVideo2">{convertDateFormat(new Date(noticia1DateCreated))} (CDT)</p>
                      </button>

                      <button className="AvisoItemViejo3" onClick={openModalNoticia2}>
                        <p className="TitleVideo3">🎥 New company video</p>
                        <p className="TimeVideo3">{moment(noticia2DateCreated).fromNow()}</p>
                        <p className="DateVideo3">{convertDateFormat(new Date(noticia2DateCreated))} (CDT)</p>
                      </button>
                  </div>

                  <div className="BoxSocialMediaContent">
                      {/* Renders the INSTRAGRAM widget, sending the props client */}
                      <SocialMediaWidget client={client}/>
                  </div>

                  <div  className="BoxYoutubeVideos">
                      {/* Renders the YOUTUBE widget, sending the props client */}
                      <YouTubeWidget client={client}/>
                  </div>


              </div>
            </div>





          <Modal isOpen={isOpenModalPrePub} closeModal={closeModalPrePub}>
              <div className="modal-content">
                  {/* Reders the PrePublicacion component */}
                  <PrePublicacion prePublicacion={props.prePublicacion} datePrePub={convertDateFormat(new Date(prePubDateCreated))} />
                <button className="close-modal" onClick={closeModalPrePub}>
                  <div className="buttonText">CLOSE</div>
                </button>
              </div>
          </Modal>



          <Modal isOpen={isOpenModalPostPub} closeModal={closeModalPostPub}>
              <div className="modal-content">
                  {/* Reders the PostPublicacion component */}
                  <PostPublicacion postPublicacion={props.postPublicacion} datePostPub={convertDateFormat(new Date(postPubDateCreated))} />
                <button className="close-modalPostPub" onClick={closeModalPostPub}>
                  <div className="buttonText">CLOSE</div>
                </button>
              </div>
          </Modal>


          <Modal isOpen={isOpenModalNoticia1} closeModal={closeModalNoticia1}>
              <div className="modal-content">
                  {/* Reders the PrePublicacion component */}
                  <Noticia noticia={props.noticia} recentNews={0} />
                <button className="close-modalNoticia" onClick={closeModalNoticia1}>
                  <div className="buttonText">CLOSE</div>
                </button>
              </div>
          </Modal>


          <Modal isOpen={isOpenModalNoticia2} closeModal={closeModalNoticia2}>
              <div className="modal-content">
                  {/* Reders the PrePublicacion component */}
                  <Noticia noticia={props.noticia} recentNews={1} />
                <button className="close-modalNoticia" onClick={closeModalNoticia2}>
                  <div className="buttonText">CLOSE</div>
                </button>
              </div>
          </Modal>

          </div>
        : null }

      </div>
    )
  }


export default BandejaDeAvisos;
