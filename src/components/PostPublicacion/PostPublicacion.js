import React from 'react';
import cookies from '../../Images/functionalCookies.png';
import clearBrowser from '../../Images/clear-browsing-data2.png';
import './PostPublicacion.css';
import moment from 'moment';

const PostPublicacion = (props) => {

    //recibimos las props (que es un objeto con toda la informacion de postPublicacion)
    //Declaramos la variable postPublicacion que ahora tiene el valor del objeto (props)
    const postPublicacion = props.postPublicacion;

    return (

        <div>
            {/* Mientras el objeto recibido no sea cero o null se renderea ó se muestra en la pantalla todo la siguiente informacion*/}
            { Object.keys(postPublicacion).length !== 0  ?

                <div className="PostPublicacion">

                    <div className="Head">
                        <p className="Forwarder">Hello, Freight Forwarder</p>
                        <p className="PostTime">{moment(props.postPublicacion.fh_comunicado).fromNow()}</p>
                        <div className="Hairline"></div>
                    </div>

                    <img className="CookieImage" src={cookies}></img>

                    {/* Para mostrar el titlo se usa el objeto en su valor tx_titulo y lo mismo para el subtitulo  */}
                    <p className="Title"> {postPublicacion.tx_titulo} </p>
                    <p className="DatePostPub">{props.datePostPub}</p>
                    <p className="Subtitle"> {postPublicacion.tx_subTitulo} </p>
                   <img className="Shotscreen" src={clearBrowser}></img>

                   <div className="DeleteCookiesText">
                        <p> Go to the <strong>three-dot menu</strong> at the upper right of Chrome to select <strong>More tools > Clear browsing data.</strong></p>


                        <p>This will open a dialog box to delete your browsing browsing, as well as your download history (it won't delete the actual downloaded files), cookies, cached images and files (which help load pages faster when you revisit), saved passwords, and more.</p>

                        <p>You can delete only the info from the last hour, day, week, month, or all of it from "the beginning of time."</p>
                    </div>

                   <div className="Foot">
                        <div className="HairlineBottom"></div>

                    </div>


                </div>

            : null }

        </div>


    )
}

export default PostPublicacion;
